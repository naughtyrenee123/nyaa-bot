{-# LANGUAGE FlexibleContexts #-}

module SyncRules (eventHandler) where

import BasicPrelude
import Control.Monad.Trans.Maybe (MaybeT, runMaybeT)

import qualified Data.Map.Strict as DMS
import qualified Data.Text as T (singleton)
import Discord
import qualified Discord.Requests as R
import Discord.Types as T
import qualified Prelude as P
import Text.Parsec (Parsec, between, parse, many, many1, anyChar, try)
import Text.Parsec.Char (digit, char, string)

import AppM
import Config
import Katip


eventHandler :: Event -> AppM ()
eventHandler (MessageCreate msg)       | messageChannelId msg == rulesChannel = syncRules'
eventHandler (MessageUpdate cid _)     | cid == rulesChannel                  = syncRules'
eventHandler (MessageDelete cid _)     | cid == rulesChannel                  = syncRules'
eventHandler (MessageDeleteBulk cid _) | cid == rulesChannel                  = syncRules'
eventHandler _ = pure ()

syncRules' :: AppM ()
syncRules' = void $ runMaybeT syncRules

syncRules :: MaybeT AppM ()
syncRules = do
  -- clear old rules
  $(logTM) InfoS "Clearing #rules-new-users"
  badRules <- liftDiscordWithErrorHandling "Fetch mirrored rules"
              . restCall
              $ R.GetChannelMessages mirroredRulesChannel (100, R.LatestMessages)

  -- Have to do these one by one because bulk deletes are only allowed for messages <14d old
  forM_ badRules $ \msg ->
    liftDiscordWithErrorHandling_ "Delete mirrored rules"
      . restCall
      $ R.DeleteMessage (mirroredRulesChannel, messageId msg)

  $(logTM) InfoS "Uploading #rules-new-users"
  newRules <- liftDiscordWithErrorHandling "Fetch mirrored rules"
              . restCall
              $ R.GetChannelMessages rulesChannel (100, R.LatestMessages)

  forM_ (reverse newRules) $ \msg -> do
    msg' <- transformMsg $ messageContent msg

    liftDiscordWithErrorHandling_ "Recreate mirrored rules"
      . restCall
      . R.CreateMessage mirroredRulesChannel
      $ msg'

-- Translate channel mentions from <#1234> -> #channel-name, since the embeds don't have visible names to new users who are unable to 
transformMsg
  :: (MonadDiscord m, KatipContext m)
  => Text -> m Text
transformMsg msg = transformChannelMentions f msg where
  f cid = do
    channel <- DMS.lookup cid
               .   cacheChannels
               <$> liftDiscord readCache

    case channel of
         Nothing -> $(logTM) WarningS ("Failed to resolve channel ID: " <> showLS cid) >> pure msg
         Just c  -> return $ "`#" <> channelName c <> "`"

transformChannelMentions
  :: (KatipContext m)
  => (ChannelId -> m Text) -> Text -> m Text
transformChannelMentions render msg = do
  -- <#1234>
  let channelMention
        :: Parsec Text () ChannelId
        =   between (string "<#") (char '>')
        $   DiscordId
        .   Snowflake
        .   P.read
        <$> many1 digit

      p :: Parsec Text () [Either Char ChannelId]
        = many ((Right <$> try channelMention) <|> (Left <$> anyChar))

  case parse p "" msg of
       -- On parse error, log and fallback to using unmodified message
       Left e -> do
         $(logTM) WarningS ("Failed to parse rule message: " <> showLS e)
         pure msg
       Right tokens -> map concat
                       . mapM (either (pure . T.singleton) render)
                       $ tokens

