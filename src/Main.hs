{-# LANGUAGE FlexibleContexts #-}

module Main where

import BasicPrelude
import Control.Monad.Trans.Maybe (MaybeT(..), runMaybeT)
import Data.Acid (openLocalStateFrom, closeAcidState)
import qualified Data.Map.Strict as DMS
import qualified Data.Text as T
import Data.Time.Clock (getCurrentTime, diffUTCTime, secondsToNominalDiffTime)
import Safe (headMay)
import System.Environment (getEnv)
import System.IO (stderr)
import UnliftIO.Exception (bracket)

import Discord
import qualified Discord.Requests as R
import Discord.Types as T
import Katip

import AppM
import Config
import DiscordExtras
import qualified MiscCommands as MC
import qualified SyncRules as SR
import qualified VoiceConsentEnforcer as VCE


main :: IO ()
main = do
    let mkLogEnv = do
            fileScribe <- mkFileScribe "/opt/nyaa-bot/nyaa-bot.log" (permitItem DebugS) V2
            stdErrScribe <- mkHandleScribe ColorIfTerminal stderr (permitItem DebugS) V2

            registerScribe "file" fileScribe defaultScribeSettings
                =<< registerScribe "stderr" stdErrScribe defaultScribeSettings
                =<< initLogEnv "nyaa-bot" "production"

    bracket mkLogEnv closeScribes $ \logEnv ->
      bracket (openLocalStateFrom "/opt/nyaa-bot" def) closeAcidState $ \acid -> do
        let withLogging = runKatipContextT logEnv () "main"
        let runAppM' = runAppM logEnv acid

        botToken <- T.pack <$> getEnv "BOT_TOKEN"

        let eventHandler' e
              = sequence_ [
                  eventHandler e,
                  MC.eventHandler e,
                  SR.eventHandler e,
                  VCE.eventHandler e
                ]

        userFacingError <- runDiscord $ def { discordToken = botToken,
                                              discordOnStart = runAppM' $ $(logTM) InfoS "Started",
                                              discordOnEnd = withLogging $ $(logTM) InfoS "Terminating...",
                                              discordOnEvent = runAppM' . eventHandler' }
        withLogging $ $(logTM) WarningS (logStr userFacingError)

-- Main event processing loop
eventHandler :: Event -> AppM ()
-- Log enabled servers
eventHandler (GuildCreate guild) = do
    $(logTM) InfoS $ concat [
            "Guild \"",
            logStr $ guildName guild,
            "\" (",
            showLS (guildId guild),
            ") connected"
        ]

-- Message events - for parsing commands
eventHandler (MessageCreate msg@Message{..}) = void . runMaybeT $ do
    let txt = T.toLower messageContent
        txtContainsAnyOf = any (flip T.isInfixOf $ txt)

    discordCache <- liftDiscord readCache
    guildId <- hoistMaybe messageGuildId
    let thisGuild = DMS.lookup guildId $ cacheGuilds discordCache

    -- Tell the mods when someone is done reading the rules
    when (and [
            T.isInfixOf "rules" txt,
            messageChannelId == waitingRoomChannel,
            not (userIsBot messageAuthor)
          ])
      $ do

        -- Need to explicitly fetch this, because the GuildMember sub-object isn't populated for MessageCreate
        authorInfo <-
          liftDiscordWithErrorHandling "Query message author"
            . restCall
            $ R.GetGuildMember guildId (userId messageAuthor)

        when (userIsMod authorInfo) $ fail ""

        $(logTM) DebugS $ concat [
            "User ",
            showLS messageAuthor,
            " has read the rules"
          ]

        liftDiscordWithErrorHandling_ "Rules read notification"
          . restCall
          . R.CreateMessage userLogChannel
          $ concat [
              "@here - ",
              mentionUser (userId messageAuthor),
              " in ",
              mentionChannel waitingRoomChannel,
              " has read the rules"
            ]
        fail ""

    -- Ignore if it doesn't mention us (by user ID or role)
    when (null messageMentions && null messageMentionRoles) $ fail ""


    let botUser = userId $ cacheCurrentUser discordCache
    let botRole :: RoleId
          = maybe 0 head
          . map (map roleId
                . filter ((==) "nyaa-bot" . roleName)
                . guildRoles
                )
          $ thisGuild

    unless (botUser `elem` map userId messageMentions || botRole `elem` messageMentionRoles)
      $ fail ""

    -- Parse the command - the first word will typically be <@!820340010321838150>
    $(logTM) DebugS $ "Received mention: " <> showLS messageContent

    VCE.handleCommand msg $ words txt
    MC.handleCommand msg $ words txt

    when (txtContainsAnyOf ["bad", "fuck", "dirty", "depraved", "whore", "slut"]) $ do
        liftDiscordWithErrorHandling_ "Bad bot notification"
          . restCall
          $ R.CreateMessage messageChannelId ";-;"
        fail ""

    when (T.isInfixOf "pet me" txt) $ do
        liftDiscordWithErrorHandling_ "Good human notification"
          . restCall
          $ R.CreateMessage messageChannelId "Good human! *pets*"
        fail ""


    when (T.isInfixOf "lick" txt) $ do
        liftDiscordWithErrorHandling_ "Lick notification"
          . restCall
          $ R.CreateMessage messageChannelId "*licks back* :3"
        fail ""

    when (txtContainsAnyOf ["good", "best", "nyaa", "pat", "pet"]) $ do
        liftDiscordWithErrorHandling_ "Good bot notification"
          . restCall
          $ R.CreateMessage messageChannelId "Nyaa! :3 :3 :3"
        fail ""

-- For logging when the channel order changes
eventHandler (ChannelUpdate updatedChannel) = void . runMaybeT $ do
    unless (channelGuild updatedChannel == lesbdsmServerId)
      $ fail ""

    discordCache <- liftDiscord readCache
    guildInfo    <- hoistMaybe
                      . DMS.lookup (channelGuild updatedChannel)
                      $ cacheGuilds discordCache

    oldChan <- hoistMaybe
                . headMay
                . filter (on (==) channelId updatedChannel)
                . fromMaybe []
                . guildChannels
                $ guildInfo

    when (channelPosition oldChan == channelPosition updatedChannel)
      $ fail ""

    liftDiscordWithErrorHandling_ "Channel moved"
      . restCall
      . R.CreateMessage dynoLogChannel
      $ concat [
          "Channel ",
          mentionChannel (channelId updatedChannel),
          " had position changed: ",
          tshow (channelPosition oldChan),
          " -> ",
          tshow (channelPosition updatedChannel)
        ]

-- Log changes to threads
eventHandler (ThreadCreate thread) = void . runMaybeT $ do
    handleThreadEvent thread "created"

eventHandler (ThreadDelete thread) = void . runMaybeT $ do
    handleThreadEvent thread "deleted"

eventHandler (ThreadUpdate thread) = void . runMaybeT $ do
    unless (channelGuild thread == lesbdsmServerId)
      $ fail ""

    $(logTM) DebugS $ "Received ThreadUpdate for: " <> showLS thread

    -- HACK: Figuring out the past state is hard, but the archival time gets updated when the thread is archived or unarchived, so we just look at that
    isArchived  <- hoistMaybe $ threadMetadataArchived <$> channelThreadMetadata thread
    archiveTime <- hoistMaybe $ threadMetadataArchiveTime <$> channelThreadMetadata thread
    now <- liftIO $ getCurrentTime
    let delta = diffUTCTime now archiveTime

    $(logTM) DebugS $ "ThreadUpdate archival age: " <> showLS delta

    -- We typically get the event 120 ms after, so allowing a healthy margin
    unless (delta < secondsToNominalDiffTime 0.5)
      $ fail ""

    if isArchived
       then handleThreadEvent thread "archived"
       else handleThreadEvent thread "unarchived"

 -- eventHandler e = $(logTM) DebugS $ "Received event: " <> showLS e
eventHandler _ = pure ()


handleThreadEvent :: Channel -> Text -> MaybeT AppM ()
handleThreadEvent thread event = do
    unless (channelGuild thread == lesbdsmServerId)
      $ fail ""

    let parentInfo
          = case channelParentId thread of
                 Just pid -> "in " <> mentionChannel (DiscordId . unId $ pid) <> " "
                 Nothing  -> ""

        text = concat [
                  mentionChannel (channelId thread),
                  " ",
                  parentInfo,
                  "with name '",
                  (fromMaybe "(null)" $ channelThreadName thread),
                  "'"
                ]

        colour = if event `elem` ["deleted", "archived"]
                    then DiscordColorRed
                    else DiscordColorDarkBlue

        embed = def {
                  createEmbedTitle = "Thread " <> event,
                  createEmbedUrl = channelUrl (channelGuild thread) (channelId thread),
                  createEmbedDescription = text,
                  createEmbedColor = Just colour
                }

    liftDiscordWithErrorHandling_ ("Thread " <> showLS event)
      . restCall
      . R.CreateMessageDetailed dynoLogChannel
      $ def {
          R.messageDetailedEmbeds = Just [embed]
        }
    pure ()

