{-# LANGUAGE FlexibleContexts #-}

module VoiceConsentEnforcer(eventHandler, handleCommand) where

import BasicPrelude
import Control.Monad.Reader (MonadReader)
import Control.Monad.Trans.Maybe (MaybeT(..), runMaybeT)
import Data.Aeson (encode, eitherDecode)
import Data.Bits
import qualified Data.Map.Strict as DMS
import qualified Data.Text as T
import Safe (headMay)

import Discord
import qualified Discord.Requests as R
import Discord.Types as T
import Katip

import AcidDefinitions
import AppM
import Config
import DiscordExtras


-- Check for reacts approving access
-- Using MaybeT to enable short-circuiting exits via fail
eventHandler :: Event -> AppM ()
eventHandler (MessageReactionAdd ReactionInfo{..}) = void . runMaybeT $ do
    -- Only proceed if it's an emote we care about
    unless ((emojiName reactionEmoji) `elem` yesEmotes) $ fail ""

    $(logTM) DebugS $ concat [
        "Handling react ",
        showLS $ emojiName reactionEmoji,
        " in channel ",
        showLS reactionChannelId,
        " by user ",
        showLS reactionUserId,
        " on message ",
        showLS reactionMessageId
      ]

    -- Get message so we know who wrote it
    msg <- liftDiscordWithErrorHandling "Error querying message"
            . restCall
            $ R.GetChannelMessage (reactionChannelId, reactionMessageId)

    let approver = reactionUserId
        candidate = userId $ messageAuthor msg
        voiceChannel = messageChannelId msg

    processAllowUserRequest voiceChannel approver candidate

-- Voice events (currently not parsed by upstream, so we roll our own parsing)
eventHandler (UnknownEvent "VOICE_STATE_UPDATE" rawObject) = void . runMaybeT $ do
    vcInfo :: VoiceStateInfo
        <- logEither "Decoding event object" . eitherDecode $ encode rawObject

    $(logTM) DebugS $ concat [
        "User ",
        showLS $ voiceUserId vcInfo,
        " is ",
        case voiceChannelId vcInfo of
             Just cid -> "now in " <> showLS cid
             Nothing  -> "has disconnected from voice"
      ]

    -- Update state tracking
    oldChannel <- appQuery $ GetUserChannel (voiceUserId vcInfo)

    case voiceChannelId vcInfo of
         Just cid -> appUpdate $ UpdateUserChannel (voiceUserId vcInfo) cid
         Nothing  -> appUpdate $ RemoveUserChannel (voiceUserId vcInfo)

    oldChannelCount <- channelMemberCount oldChannel
    newChannelCount <- channelMemberCount (voiceChannelId vcInfo)

    -- User left old channel and it's now empty, so reset permissions
    when (isJust oldChannel && oldChannelCount == 0) $ do
      let Just oldChannel' = oldChannel
      unlockVC oldChannel' "Everyone has left voice - channel is now unlocked."

    -- This is the first person to enter the channel - need to lock
    when (newChannelCount == 1) $ do
      let Just newChannel = voiceChannelId vcInfo

      when (newChannel `elem` autoLockChannels)
        . lockVC newChannel
        $  "This voice channel now requires consent to join. To grant consent, please react to the request in this channel with :thumbsup: or use `@nyaa-bot allow USER`.\n"
        <> "You can disable this with `@nyaa-bot unlock`."

eventHandler (MessageCreate Message{..}) = void . runMaybeT $ do
    let txt = T.toLower messageContent

    when (T.isInfixOf "can i join" txt && not (userIsBot messageAuthor)) $ do
      isLocked <- isVCLocked messageChannelId
      unless isLocked
        . liftDiscordWithErrorHandling_ "Channel is unlocked"
        . restCall
        . R.CreateMessage messageChannelId
        $ concat [
              mentionUser (userId messageAuthor),
              " the channel is unlocked - come on in ^_^"
            ]

eventHandler _ = pure ()


handleCommand :: Message -> [Text] -> MaybeT AppM ()
handleCommand Message{..} [_, "unlock"] = do
    checkUserCanGrantAccess messageChannelId (userId messageAuthor)
    unlockVC messageChannelId "Channel is now unlocked - everyone come on in!\nUse `@nyaa-bot lock` to re-enable enforced consent."

handleCommand Message{..} [_, "lock"] = do
    checkUserCanGrantAccess messageChannelId (userId messageAuthor)
    lockVC messageChannelId "Channel is now locked - consent will now be required to join.\nUse `@nyaa-bot unlock` to disable this."

handleCommand msg@Message{..} (_:"allow":_) = do
    let approver = userId messageAuthor

    if null (userMentions msg)
       then liftDiscordWithErrorHandling_ "Invalid allow command"
            . restCall
            $ R.CreateMessage messageChannelId "Silly human - please specify a person to let into the voice channel.\ne.g. `@nyaa-bot allow @Sappho`"
       else processAllowUserRequest messageChannelId approver `mapM_` (userMentions msg)

  -- Move users to sleepy time VC
handleCommand msg@Message{..} (_:"sleepy":_) = do
    guildId <- hoistMaybe messageGuildId

    if null (userMentions msg)
       then liftDiscordWithErrorHandling_ "Invalid sleepy command"
            . restCall
            $ R.CreateMessage messageChannelId "If you're sleepy, why not join the Sleepytime VC?"
       else liftDiscordWithErrorHandling_ "Move users" $ do
              (restCall
                . (\uid -> R.ModifyGuildMember guildId uid (def { R.modifyGuildMemberOptsMoveToChannel = Just sleepyChannel }))
                ) `mapM_` (userMentions msg)
              restCall $ R.CreateMessage messageChannelId "Sweet dreams ^_^"

handleCommand Message{..} [_, "ping"] = do
    currentMembers <- appQuery $ GetChannelMembers messageChannelId

    when (not $ null currentMembers)
      . liftDiscordWithErrorHandling_ "Pinging VC members"
      . restCall
      . R.CreateMessage messageChannelId
      . intercalate ", "
      $ map mentionUser currentMembers

handleCommand Message{..} [_, "migrate", dstTxtMention] = do
    dstVC <- logEither ("Parsing mention: " <> showLS dstTxtMention) $ parseMention dstTxtMention
    let srcVC = messageChannelId
    guild <- hoistMaybe messageGuildId
    currentMembers <- appQuery $ GetChannelMembers srcVC

    when (not $ null currentMembers)
      . liftDiscordWithErrorHandling_ "Migrate users" $ do
          (restCall
            . (\uid -> R.ModifyGuildMember guild uid (def { R.modifyGuildMemberOptsMoveToChannel = Just dstVC }))
            ) `mapM_` currentMembers
          restCall $ R.CreateMessage messageChannelId "See you on the other side ^_^"

handleCommand _ _ = pure ()


-- Unlock a voice channel, allowing anyone to join it
unlockVC
  :: (MonadDiscord m, MonadFail m, KatipContext m)
  => ChannelId -> Text -> m ()
unlockVC voiceChannel msg = do
    $(logTM) InfoS $ concat [
        "Unlocking VC ",
        showLS voiceChannel
      ]

    -- we reset perms by removing the member-specific grants and setting connect perms on roles
    sendNotif <- modifyChannelPerms voiceChannel
      $ modifyAllowsForToggleableRoles ((.|.) dpConnect)
      . filter ((==) overwriteForRole . overwriteType)

    when sendNotif
      . liftDiscordWithErrorHandling_ "Voice empty notification"
      . restCall
      $ R.CreateMessage voiceChannel msg

-- Lock a voice channel, so that users need to go through the consent request flow
lockVC
  :: (MonadDiscord m, MonadFail m, KatipContext m, MonadReader AppContext m)
  => ChannelId -> Text -> m ()
lockVC voiceChannel msg = do
    $(logTM) InfoS $ concat [
        "Locking VC ",
        showLS voiceChannel
      ]

    -- We lock by unsetting connect perms on roles
    sendNotif <- modifyChannelPerms voiceChannel
      $ modifyAllowsForToggleableRoles ((.&.) (complement dpConnect))

    -- Grant anyone already in the channel access, so that if their connection drops out they can easily reconnect
    currentMembers <- appQuery $ GetChannelMembers voiceChannel
    let newPerms = grantUserVoiceAccess <$> currentMembers

    _ <- modifyChannelPerms voiceChannel $ (++) newPerms

    when sendNotif
      . liftDiscordWithErrorHandling_ "Voice in use notification"
      . restCall
      $ R.CreateMessage voiceChannel msg

-- Business logic for granting or rejecting an attempt to give a user access to a voice channel
processAllowUserRequest :: ChannelId -> UserId -> UserId -> MaybeT AppM ()
processAllowUserRequest voiceChannel approver candidate = do
    -- Check if we're even in a voice channel
    channel <- hoistMaybe
               =<< DMS.lookup voiceChannel
               .   cacheChannels
               <$> liftDiscord readCache
    unless (isVoiceChannel channel)
      $ fail ""

    -- Check if the the user is actually authorized to grant access
    checkUserCanGrantAccess voiceChannel approver

    -- Check if the user tried to grant access to the bot
    botUser <- userId . cacheCurrentUser <$> liftDiscord readCache

    when (candidate == botUser) $ do
      liftDiscordWithErrorHandling_ "User tried to grant access to bot"
        . restCall
        $ R.CreateMessage voiceChannel
        $ concat [
            "Silly human - I grant access to you, not the other way around. :3\n",
            "You're supposed to react on the message someone posts to ask for access, not mine."
          ]
      fail ""

    $(logTM) InfoS $ concat [
            "User ",
            showLS approver,
            " approved user ",
            showLS candidate,
            " for access to channel ",
            showLS voiceChannel
        ]

    -- Update perms
    -- Note that because we only grant access by adding user-specific perms, we can later reset perms by just dropping all non-role perms
    sendNotif <- modifyChannelPerms voiceChannel $ (:) (grantUserVoiceAccess candidate)

    -- TODO: ideally we'd use the reply feature, but CreateMessage constructs the JSON blob for us so we'd need to create a new API for doing so
    -- See: https://hackage.haskell.org/package/discord-haskell-1.8.3/docs/src/Discord.Internal.Rest.Channel.html#local-6989586621679139873
    when sendNotif
      . liftDiscordWithErrorHandling_ "User granted access"
      . restCall
      $ R.CreateMessage voiceChannel
      $ concat [
          ":white_check_mark: ",
          mentionUser candidate,
          " you have been granted access by ",
          mentionUser approver,

          -- Easter egg
          fromMaybe " - come on in ^_^"
          $ lookup candidate [
              (kawaUID,  ".\nWelcome back, Hime-sama!"),
              (jennyUID, ".\nI sense a great disturbance in the force, as if a million subs cried out and were suddenly denied."),
              (blakeUID, ".\nWe are now at BRATCON 1."),
              (skyeUID,  ".\nChomp! :bear:"),
              (myceUID,  ".\n*gives the good bun lots of pets* :rabbit:")
            ]
        ]

-- Helper function for modifying channel permissions
-- Returns true if this actually changed the perms, or false if it was a no-op.
modifyChannelPerms
  :: (MonadDiscord m, MonadFail m, KatipContext m)
  => ChannelId -> ([Overwrite] -> [Overwrite]) -> m Bool
modifyChannelPerms cid permFunc = do
  channelInfo <- liftDiscordWithErrorHandling ("Fetching channel info for " <> showLS cid)
    . restCall
    $ R.GetChannel cid

  let newPerms = permFunc $ channelPermissions channelInfo

  liftDiscordWithErrorHandling_ ("Updating channel perms for " <> showLS cid)
    . restCall
    . R.ModifyChannel cid
    $ def { R.modifyChannelPermissionOverwrites = Just newPerms }

  return $ (channelPermissions channelInfo) /= newPerms

-- Helper function for getting the number of users in a channel
channelMemberCount ::(MonadReader AppContext m, MonadIO m) => Maybe ChannelId -> MaybeT m Int
channelMemberCount = maybe (pure 0) (map length . appQuery . GetChannelMembers)

-- Lifts the specified mutation to allows for toggleable roles to the level of Overwrites, for use with modifyChannelPerms
modifyAllowsForToggleableRoles :: (Integer -> Integer) -> ([Overwrite] -> [Overwrite])
modifyAllowsForToggleableRoles allowMod = res where
  res = mapIf (flip elem rolesToToggle . overwriteRoleId)
              (\o -> o { overwriteAllow = tshow . allowMod . read $ overwriteAllow o })

  -- Applies a mapping function (f) only to elements satisfying the predicate (p)
  mapIf p f = map f' where
    f' x | p x       = f x
         | otherwise = x

-- Performs permission checking, invoking fail if the user does not have access
checkUserCanGrantAccess
  :: (MonadDiscord m, MonadFail m, KatipContext m, MonadReader AppContext m)
  => ChannelId -> UserId -> m ()
checkUserCanGrantAccess voiceChannel approver = do
    approverChannel <- appQuery $ GetUserChannel approver

    when (Just voiceChannel /= approverChannel) $ do
      liftDiscordWithErrorHandling_ "Approver failed authorization"
        . restCall
        $ R.CreateMessage voiceChannel
        $ concat [
            ":warning: ",
            mentionUser approver,
            " you cannot manage access to the channel as you are not in it."
          ]
      fail ""

isVCLocked
  :: (MonadDiscord m, MonadFail m, KatipContext m, MonadReader AppContext m)
  => ChannelId -> m Bool
isVCLocked cid = do
  channelPerms <- map channelPermissions
                  . liftDiscordWithErrorHandling "Get channel"
                  . restCall
                  $ R.GetChannel cid
  let testRole = head rolesToToggle
      testPerms
        = fromMaybe (0 :: Integer)
        . headMay
        . map read
        . map overwriteAllow
        . filter ((==) testRole . overwriteRoleId)
        . filter ((==) overwriteForRole . overwriteType)
        $ channelPerms

  return $ testPerms .&. dpConnect == 0

-- Creates a perms overwrite for granting a user the ability to connect to a voice channel
grantUserVoiceAccess :: UserId -> Overwrite
grantUserVoiceAccess uid = Overwrite uid' overwriteForMember (tshow dpConnect) "0" where
  uid' = DiscordId . unId $ uid

userMentions :: Message -> [UserId]
userMentions
  = map userId
  . filter (not . userIsBot)
  . messageMentions

