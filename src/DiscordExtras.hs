{-# OPTIONS_GHC -Wno-orphans #-}

-- Extra logic for working with discord-haskell library
module DiscordExtras where

import BasicPrelude
import Data.Aeson (FromJSON(parseJSON), withObject, (.:), (.:?))
import Data.Char (isDigit)
import Discord
import Discord.Types
import qualified Prelude as P
import Text.Parsec (ParseError, between, parse, many1, satisfy)
import Text.Parsec.Char (digit, char, string)

-- Represents a *change* in the state of the voice call. i.e. a user joining, disconnecting, or moving
-- Note that the events for join and move are identical - they can only be distinguished between by tracking state
-- https://discord.com/developers/docs/resources/voice#voice-state-object
data VoiceStateInfo = VoiceStateInfo {
  voiceGuildId :: GuildId,
  voiceChannelId :: Maybe ChannelId,            -- Nothing if user has left, Just(..) if the user has joined/moved channels
  voiceUserId :: UserId,                        -- redundant given voiceMember
  voiceMember :: GuildMember,
  voiceSession :: SessionId                     -- seems to be specific to a single user's connection to the channel
  -- deafen, mute, etc. fields omitted for now
} deriving (Eq, Ord, Show)

instance FromJSON VoiceStateInfo where
  parseJSON = withObject "VoiceStateInfo" $ \o ->
    VoiceStateInfo <$> o .:  "guild_id"
                   <*> o .:? "channel_id"
                   <*> o .:  "user_id"
                   <*> o .:  "member"
                   <*> o .:  "session_id"

newtype SessionId = SessionId Text
  deriving (Eq, Ord, Show, FromJSON)

-- From https://discord.com/developers/docs/topics/permissions#permissions-bitwise-permission-flags

-- Allows joining a voice channel
dpConnect :: Integer
dpConnect = 0x00100000

mentionUser :: UserId -> Text
mentionUser uid = "<@" <> tshow uid <> ">"

mentionRole :: RoleId -> Text
mentionRole rid = "<@&" <> tshow rid <> ">"

mentionChannel :: ChannelId -> Text
mentionChannel cid = "<#" <> tshow cid <> ">"

-- Parses one of the above mentions to the underlying ID
parseMention :: forall a. Text -> Either ParseError (DiscordId a)
parseMention = parse p "" where
  num = DiscordId . Snowflake . P.read <$> many1 digit
  notDigit = satisfy (not . isDigit)
  p = between (char '<') (char '>')
    $ many1 notDigit >> num

-- Parses a msg link. e.g. https://discord.com/channels/607486855516258314/643006848600899594/828261953918730271
parseMsgLink :: Text -> Either ParseError (GuildId, ChannelId, MessageId)
parseMsgLink = parse p "" where
  num = DiscordId . P.read <$> many1 digit
  p = do
        void $ string "https://discord.com/channels/"
        a <- num
        void $ char '/'
        b <- num
        void $ char '/'
        c <- num

        pure (a, b, c)

channelUrl :: GuildId -> ChannelId -> Text
channelUrl gid cid = concat [
    "https://discord.com/channels/",
    tshow gid,
    "/",
    tshow cid
  ]

avatarUrl :: User -> Maybe Text
avatarUrl user = do
  let uid = tshow $ userId user
  avatar <- userAvatar user
  return $ concat [
      "https://cdn.discordapp.com/avatars/",
      uid,
      "/",
      avatar,
      ".png"
    ]

-- Constants for the Overwrite data type
overwriteForMember :: Integer
overwriteForMember = 1
overwriteForRole :: Integer
overwriteForRole = 0

-- OverwriteId can reference a role or user ID, so this function coerces the type
overwriteRoleId :: Overwrite -> RoleId
overwriteRoleId = DiscordId . unId . overwriteId

-- True if a channel is a voice channel
isVoiceChannel :: Channel -> Bool
isVoiceChannel ChannelVoice{} = True
isVoiceChannel _ = False

