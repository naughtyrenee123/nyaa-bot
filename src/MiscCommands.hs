{-# LANGUAGE FlexibleContexts #-}

module MiscCommands where

import BasicPrelude
import Control.Monad.Extra (whenM)
import Control.Monad.Reader (MonadReader)
import Control.Monad.Trans.Maybe (MaybeT, runMaybeT)
import qualified Data.Map.Strict as DMS
import Data.Maybe (fromJust)
import qualified Data.Text as T
import Safe (headMay)

import Discord
import qualified Discord.Requests as R
import Discord.Types as T
import Katip

import AcidDefinitions
import AppM
import Config
import DiscordExtras
import Resources
import RolePreviewTypes


eventHandler :: Event -> AppM ()
eventHandler (MessageReactionAdd react)
  = void
  . runMaybeT
  . whenM (isWaitingRoomReact "add" react)
  $ toggleNewFriend react True

eventHandler (MessageReactionRemove react)
  = void
  . runMaybeT
  . whenM (isWaitingRoomReact "remove" react)
  $ toggleNewFriend react False

eventHandler _ = pure ()


handleCommand :: Message -> [Text] -> MaybeT AppM ()
handleCommand Message{..} [_, "dance"] = do
    -- Convenience command for Calico so that this doesn't need to be CW'd every time
    liftDiscordWithErrorHandling_ "Posting dance GIF"
    . restCall
    . R.CreateMessageDetailed messageChannelId
    $ def {
        R.messageDetailedContent = "CW fast-moving image",
        R.messageDetailedFile = Just ("SPOILER_dance.gif", danceGif)
      }

handleCommand Message{..} [_, "chomp"] = do
    -- Chomp method for SkyeBear
    liftDiscordWithErrorHandling_ "Proceeding to chomp"
    . restCall $ R.CreateMessage messageChannelId "Ouch! That hurt! :adhesive_bandage:"

handleCommand Message{..} [_, "hug"] = do
    -- Hug method for SkyeBear
    liftDiscordWithErrorHandling_ "Hug inbound"
    . restCall $ R.CreateMessage messageChannelId "*huggles* <3"

handleCommand Message{..} [_, "debug", "state"] = do
    checkAdminAccess messageChannelId (userId messageAuthor)
    appDB <- appQuery $ GetAppDB

    liftDiscordWithErrorHandling_ "Debug state message post"
      . restCall
      . R.CreateMessage messageChannelId
      $ tshow appDB

handleCommand Message{..} [_, "debug", "reacts", chanId@_, msgId@_] = do
    checkAdminAccess messageChannelId (userId messageAuthor)

    msg <- liftDiscordWithErrorHandling "Debug reacts message get"
            . restCall
            $ R.GetChannelMessage (read chanId, read msgId)

    liftDiscordWithErrorHandling_ "Debug reacts message post"
      . restCall
      . R.CreateMessage messageChannelId
      $ tshow (messageReactionEmoji <$> T.messageReactions msg)

handleCommand Message{..} [_, "debug", "msg", msgUrl@_] = do
    checkAdminAccess messageChannelId (userId messageAuthor)
    (_, chanId, msgId) <- logEither ("Parsing msg link: " <> showLS msgUrl) $ parseMsgLink msgUrl

    msg <- liftDiscordWithErrorHandling "Debug msg get"
            . restCall
            $ R.GetChannelMessage (chanId, msgId)

    let msgData = tshow msg
        isLongMsg = T.length msgData > 2000

    when isLongMsg
      $ $(logTM) DebugS ("Debug message output: " <> showLS msg)

    liftDiscordWithErrorHandling_ "Debug reacts message post"
      . restCall
      . R.CreateMessage messageChannelId
      $ (if isLongMsg
          then "<output too long, see logs>"
          else msgData
        )

handleCommand Message{..} [_, "deref", msgUrl@_] = do
    (_, chanId, msgId) <- logEither ("Parsing msg link: " <> showLS msgUrl) $ parseMsgLink msgUrl

    msg <- liftDiscordWithErrorHandling "Debug msg get"
            . restCall
            $ R.GetChannelMessage (chanId, msgId)

    let embed = def {
                  createEmbedAuthorName = userName $ T.messageAuthor msg,
                  createEmbedAuthorUrl = fromMaybe "" $ avatarUrl (T.messageAuthor msg),
                  createEmbedDescription = T.messageContent msg
                }

    liftDiscordWithErrorHandling_ "Post dereferenced message"
      . restCall
      . R.CreateMessageDetailed messageChannelId
      $ def {
          R.messageDetailedContent = "Dereferenced message by " ++ (mentionUser . userId $ T.messageAuthor msg),
          R.messageDetailedEmbeds = Just [embed],
          R.messageDetailedReference = Just (MessageReference (Just messageId) (Just messageChannelId) (messageGuildId) True)
        }

handleCommand msg [_, "deref"] = do
    ref <- hoistMaybe $ messageReferencedMessage msg
    msgUrl <- hoistMaybe
              $ headMay
              . filter (T.isPrefixOf "https://discord.com/channels/")
              . words
              . messageContent
              $ ref
    handleCommand msg ["", "deref", msgUrl]

-- Generates a link to the role preview site, which helps to identify role colour accessibility issues
handleCommand msg [_, "rolecolours"] = do
    guildId <- hoistMaybe $ messageGuildId msg

    roles <- liftDiscordWithErrorHandling "Get roles"
      . restCall
      $ R.GetGuildRoles guildId

    members <- getAllMembers guildId

    -- We only count the highest rank role for each user, since that's what determines their colour
    let roleCount :: Map RoleId Int
        roleCount = DMS.fromListWith (+)
                  . map (\k -> (k, 1))
                  . map highestRanked
                  . filter (\m -> not $ null m)
                  . map memberRoles
                  $ members

        roleRanks :: Map RoleId Integer
        roleRanks = DMS.fromList
                  . map (\r -> (roleId r, rolePos r))
                  $ roles

        highestRanked :: [RoleId] -> RoleId
        highestRanked = head . sortBy (compare `on` (negate . getRankOfRole))

        getCountOfRole roleId = fromMaybe 0 $ DMS.lookup roleId roleCount
        getRankOfRole roleId = fromMaybe 0 $ DMS.lookup roleId roleRanks

    $(logTM) InfoS $ concat [
        "Role counts: ",
        showLS . sortBy (compare `on` (negate . snd)) $ DMS.toList roleCount
      ]

    let roleUrl = rolePreviewUrl
                . filter (\r -> roleColor r /= DiscordColorDefault && getCountOfRole (roleId r) > 1)
                $ roles

    $(logTM) InfoS $ concat [
        "Generated role URL: ",
        showLS roleUrl
      ]

    -- The generated URL is 3 KB, too long to fit in a message, so we put it in a file

    liftDiscordWithErrorHandling_ "Role colour preview link"
      . restCall
      . R.CreateMessageDetailed (messageChannelId msg)
      $ def {
          R.messageDetailedContent = "URL for role preview",
          R.messageDetailedFile = Just ("link.txt", encodeUtf8 roleUrl)
        }

handleCommand _ _ = pure ()


checkAdminAccess
  :: (MonadDiscord m, MonadFail m, KatipContext m, MonadReader AppContext m)
  => ChannelId -> UserId -> m ()
checkAdminAccess textChannel uid = unless (uid == reneeUID || uid == pmUID) $ do
  liftDiscordWithErrorHandling_ "Failed admin auth check"
    . restCall
    $ R.CreateMessage textChannel
    $ "You don't have enough badges to tame me, human! >:3"
  fail ""

getAllMembers
  :: (MonadDiscord m, MonadFail m, KatipContext m, MonadReader AppContext m)
  => GuildId -> m [GuildMember]
getAllMembers guildId =  do
  let -- loop :: R.GuildMembersTiming -> _ [[GuildMember]]
      loop page = do
        res :: [GuildMember]
            <- liftDiscordWithErrorHandling "Get all members"
               . restCall
               $ R.ListGuildMembers guildId page

        if null res
           then return (res : [])
           else do

             let nextPage = R.GuildMembersTiming
                              (Just 1000)
                              (map userId
                                . memberUser
                                $ last res
                              )

             res' <- loop nextPage
             return $ res : res'

  map join
    . loop
    $ R.GuildMembersTiming (Just 1000) Nothing


isWaitingRoomReact :: LogStr -> ReactionInfo -> MaybeT AppM Bool
isWaitingRoomReact op react@ReactionInfo{..} = do
  $(logTM) DebugS $ concat [
      "React ",
      op,
      ": ",
      showLS react
    ]

  userInfo :: GuildMember <-
    liftDiscordWithErrorHandling "Query reaction user"
      . restCall
      $ R.GetGuildMember (fromJust reactionGuildId) reactionUserId

  return $ and [
      reactionGuildId == Just lesbdsmServerId,
      userIsMod userInfo,
      reactionChannelId == waitingRoomChannel,
      emojiName reactionEmoji `elem` yesEmotes
    ]

-- Updates the New Friend role for the author of the message
-- Adds it if doAdd is true, otherwise removes it
toggleNewFriend :: ReactionInfo -> Bool -> MaybeT AppM ()
toggleNewFriend react doAdd = void . runMaybeT $ do
  msgInfo <- liftDiscordWithErrorHandling "Retrieve reaction message"
              . restCall
              $ R.GetChannelMessage (waitingRoomChannel, reactionMessageId react)

  authorInfo <- liftDiscordWithErrorHandling "Query messageAuthor"
                . restCall
                $ R.GetGuildMember lesbdsmServerId (userId $ messageAuthor msgInfo)

  let roles' = if doAdd
                  then newFriendRole : memberRoles authorInfo
                  else filter (/= newFriendRole) (memberRoles authorInfo)

  liftDiscordWithErrorHandling_ "Update user roles"
                . restCall
                . R.ModifyGuildMember lesbdsmServerId (userId $ messageAuthor msgInfo)
                $ def {
                  R.modifyGuildMemberOptsRoles = Just roles'
                }

  when doAdd
    . liftDiscordWithErrorHandling_ "New user welcome message"
    . restCall
    . R.CreateMessageDetailed waitingRoomChannel
    $ def {
      R.messageDetailedContent = concat [
          "Welcome! Please add some ",
          mentionChannel roleChannel,
          -- New users don't have access to #introductions, so use a plain text name
          ", and don't forget to say hi in #introductions :slight_smile:"
        ],
      R.messageDetailedReference = Just (MessageReference
          (Just $ messageId msgInfo)
          (Just waitingRoomChannel)
          (Just lesbdsmServerId)
          True
        )
    }

  liftDiscordWithErrorHandling_ "User role change"
    . restCall
    . R.CreateMessage userLogChannel
    $ concat [
        mentionUser (userId $ messageAuthor msgInfo),
        " has had access ",
          (if doAdd
            then "granted"
            else "removed"),
        " by ",
        mentionUser (reactionUserId react)
      ]

