module Config where

import BasicPrelude
import Discord.Types (GuildId, GuildMember, ChannelId, RoleId, UserId, memberRoles)

-- Emotes that are interpreted as approval to join the chat
yesEmotes :: [Text]
yesEmotes = [
    "thumbsup", "\128077",
    "call_me", "\129305",
    "yes"
  ]

-- Automatically lock these channels when someone enters
autoLockChannels :: [ChannelId]
autoLockChannels = [
      -- LesBDSM: #nsfw-vc
      (558696081400004612),
      -- LesBDSM: #playtime fun (nsfw)
      (807620454323716128)
  ]

lesbdsmServerId :: GuildId
lesbdsmServerId = 558696081395810315

waitingRoomChannel :: ChannelId
waitingRoomChannel = 589236594226102293

userLogChannel :: ChannelId
userLogChannel = 810193609818243102

dynoLogChannel :: ChannelId
dynoLogChannel = 571796639594053652

sleepyChannel :: ChannelId
sleepyChannel = 812557695307218944

rulesChannel :: ChannelId
rulesChannel = 729268103761887252

mirroredRulesChannel :: ChannelId
mirroredRulesChannel = 848948895966167093

roleChannel :: ChannelId
roleChannel = 820110016186286130

introductionChannel :: ChannelId
introductionChannel = 558708744293187584

-- The list of roles to toggle the connect permission on
rolesToToggle :: [RoleId]
rolesToToggle = [
    558701595194556417, -- Dominant
    558701600210681857, -- Switch
    687314755295510556, -- Kink-Curious
    558701598952652821  -- Submissive
  ]

modRole :: RoleId
modRole = 558699116142788638

newFriendRole :: RoleId
newFriendRole = 600819590695616512

reneeUID :: UserId
reneeUID = 330884524030558209

myceUID :: UserId
myceUID = 270866281790570496

kawaUID :: UserId
kawaUID = 167100249272221698

jennyUID :: UserId
jennyUID = 200395680496353280

blakeUID :: UserId
blakeUID = 464796266283663360

skyeUID :: UserId
skyeUID = 221613748408942592

kathyUID :: UserId
kathyUID = 160083432548990976

pmUID :: UserId
pmUID = 129064951540219905

userIsMod :: GuildMember -> Bool
userIsMod = elem modRole
          . memberRoles

