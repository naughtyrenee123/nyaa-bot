# nyaa-bot
Private discord bot for doing random things.

[App info page](https://discord.com/developers/applications/820340010321838150/information)

[Testing channel on personal server](https://discord.com/channels/607486855516258314/643006848600899594)

## Installation
[Invite link generator](https://discord.com/developers/applications/820340010321838150/oauth2)

- Scopes: bot
- Perms: View Channels, Send Messages, Read Message History, Manage Roles
- [Invite Link](https://discord.com/api/oauth2/authorize?client_id=820340010321838150&permissions=1237237113936&redirect_uri=http%3A%2F%2Fvps-942b463d.vps.ovh.ca&scope=bot)

Note that for each voice channel managed:
  - nyaa-bot must have View Channel, Manage Channel, Manage Permissions, Connect, Move Members perms
  - @everyone must have Connect perms set to *deny*


## Deployments
Run `./deployment/deploy.sh` to deploy an updated version to the VPS. If `secrets` does not exist in the same directory, you'll need to populate it with `BOT_TOKEN=...` - get it from [here](https://discord.com/developers/applications/820340010321838150/bot).

